import { Button, Grid, TextField } from "@mui/material";
import { API, graphqlOperation } from "aws-amplify";
import React from "react";
import { Controller, useForm } from "react-hook-form";
import { CreateTaskMutationVariables } from "../API";
import { createTask } from "../graphql/mutations";
import { styled } from "@mui/material";

type FormState = {
  name: string;
  description: string;
  submit: string;
};

const TaskForm = styled("form")(({ theme }) => ({
  display: "flex",
  justifyContent: "space-between",
  alignItems: "center",
}));

const CustomGridItem = styled(Grid)(({ theme }) => ({
  display: "flex",
  alignItems: "center", // ボタンを上下中央揃えに
  "& > *": {
    margin: theme.spacing(2, 0), // ボタンとテキストフィールドの余白を調整
  },
  // ヘルパーテキストがある場合とない場合の高さを同じにする
  "& .MuiFormHelperText-root.Mui-error": {
    position: "absolute",
    top: "100%",
  },
}));

export const TaskFrom = () => {
  const {
    register,
    handleSubmit,
    control,
    formState: { errors },
  } = useForm<FormState>({
    mode: "onBlur",
    reValidateMode: "onChange",
  });

  const onSubmit = async (data: FormState) => {
    if (data.name === "") return;
    const newTask: CreateTaskMutationVariables = {
      input: {
        name: data.name,
        description: data.description,
      },
    };
    API.graphql(graphqlOperation(createTask, newTask)); // GraphQLと通信してDBにデータを保存(operation, parameter)
  };

  return (
    <TaskForm onSubmit={handleSubmit(onSubmit)}>
      <Grid container spacing={3}>
        <CustomGridItem item xs={2} md={2}>
          <Controller
            render={() => (
              <TextField
                id="task-name-field"
                label="Task Name"
                variant="outlined"
                margin="normal" // or dense
                fullWidth
                required
                helperText={errors.name?.message || ""}
                error={!!errors.name} // errorメッセージがある場合のみ表示
                {...register("name")}
              />
            )}
            defaultValue=""
            rules={{ required: "必須です。" }}
            name="name"
            control={control}
          ></Controller>
        </CustomGridItem>
        <CustomGridItem item xs={4} md={4}>
          <Controller
            render={() => (
              <TextField
                id="description-field"
                label="Description"
                variant="outlined"
                margin="normal" // or dense
                fullWidth
                helperText={errors.description?.message || ""}
                {...register("description")}
              />
            )}
            defaultValue=""
            name="name"
            control={control}
          ></Controller>
        </CustomGridItem>
        <CustomGridItem item xs={2} md={2}>
          <Controller
            render={() => (
              <Button
                variant="contained"
                color="primary"
                fullWidth
                size="large"
                onClick={handleSubmit(onSubmit)}
              >
                ADD
              </Button>
            )}
            name="submit"
            control={control}
            defaultValue=""
          ></Controller>
        </CustomGridItem>
      </Grid>
    </TaskForm>
  );
};
