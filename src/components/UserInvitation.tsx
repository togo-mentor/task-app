import { Button, Grid, styled, TextField } from "@mui/material";
import React from "react";
import { Controller, useForm } from "react-hook-form";
import { useAuth } from '../cognito/AuthContext';

type FormState = {
  email: string;
  submit: string;
};

const UserInivitationForm = styled("form")(({ theme }) => ({
  display: "flex",
  justifyContent: "space-between",
  alignItems: "center",
  width: '600px'
}));

const CustomGridItem = styled(Grid)(({ theme }) => ({
  display: "flex",
  alignItems: "center", // ボタンを上下中央揃えに
  "& > *": {
    margin: theme.spacing(2, 0), // ボタンとテキストフィールドの余白を調整
  },
  // ヘルパーテキストがある場合とない場合の高さを同じにする
  "& .MuiFormHelperText-root.Mui-error": {
    position: "absolute",
    top: "100%",
  },
}));

export const UserInvitation = () => {
  const { adminCreateUser } = useAuth()

  const {
    register,
    handleSubmit,
    control,
    formState: { errors },
  } = useForm<FormState>({
    mode: "onBlur",
    reValidateMode: "onChange",
  });

  const onSubmit = async (data: FormState) => {
    await adminCreateUser(data)
  };

  return (
    <UserInivitationForm onSubmit={handleSubmit(onSubmit)}>
      <Grid container spacing={1} justifyContent='space-around'>
        <CustomGridItem item xs={6} md={6}>
          <Controller
            render={() => (
              <TextField
                id="email-field"
                label="Email"
                variant="outlined"
                margin="normal" // or dense
                fullWidth
                helperText={errors.email?.message || ""}
                {...register("email")}
              />
            )}
            defaultValue=""
            name="email"
            control={control}
          ></Controller>
        </CustomGridItem>
        <CustomGridItem item xs={2} md={2}>
          <Controller
            render={() => (
              <Button
                variant="contained"
                color="primary"
                fullWidth
                size="large"
                onClick={handleSubmit(onSubmit)}
              >
                INVITE
              </Button>
            )}
            name="submit"
            control={control}
            defaultValue=""
          ></Controller>
        </CustomGridItem>
      </Grid>
    </UserInivitationForm>
  );
};
