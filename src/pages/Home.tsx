import React from "react";
import { Grid } from "@mui/material";
import { TaskList } from "./TaskList";
import { TaskFrom } from '../components/TaskFrom';

export type Task = {
  id: string;
  name: string;
  description: string | null;
  status: string;
};

export const Home = () => {

  return (
    <>
      <h1>Home</h1>
      <TaskFrom/>
      <Grid container spacing={1}>
        <Grid item>
          <TaskList />
        </Grid>
      </Grid>
    </>
  );
};
