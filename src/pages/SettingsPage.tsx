import { Box, Button, Grid, styled, Typography } from "@mui/material";
import React, { useState } from "react";
import Modal from "react-modal";
import { UserInvitation } from '../components/UserInvitation'

const customStyles = {
  content: {
    top: "50%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    marginRight: "-50%",
    transform: "translate(-50%, -50%)",
  },
};

const CustomGridItem = styled(Grid)(({ theme }) => ({
  border: "1px solid rgb(63, 81, 181)",
  borderRadius: theme.spacing(1),
  padding: theme.spacing(2, 3.5, 2, 3),
}));

Modal.setAppElement("#root");
export const SettingsPage = () => {
  const [isOpen, setIsOpen] = useState<boolean>(false);

  const handleOpen = () => {
    setIsOpen(true);
  };

  const handleClose = () => {
    setIsOpen(false);
  };

  return (
    <Grid container spacing={3} direction="column">
      <Grid item xs={10} md={10}>
        <Typography variant="h4">Settings</Typography>
      </Grid>
      <Grid item xs={10} sm={10}>
        <CustomGridItem
          container
          item
          xs={10}
          sm={10}
          justifyContent="space-between"
          alignItems="center"
        >
          <Typography variant="h6">ユーザーの招待</Typography>
          <Button variant="contained" color="primary" onClick={handleOpen}>
            招待する
          </Button>
          <Modal isOpen={isOpen} style={customStyles} onRequestClose={handleClose}>
            <UserInvitation/>
          </Modal>
        </CustomGridItem>
      </Grid>
    </Grid>
  );
};
